package transcriptAnalyzer;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.DataProvider;
/**
 * This class will provide testNG tests that can be used with the Transcript class.
 * @author schilling
 *
 */
public class TestTranscript {

	/**
	 * This is a transcript example that will be sued for many cases.
	 */
	private Transcript validTranscript;

	@BeforeMethod(groups = { "TestRanscript" })
	/**
	 * This method will be called before each test method and will instantiate a new instance of the class.
	 */
	public void beforeMethod() {
		validTranscript = new Transcript("Albert Einstein");
	}

	@Test(groups = { "TestRanscript" }, expectedExceptions = IllegalArgumentException.class)
	/**
	 * This test will verify that a null parameter fails the constructor.
	 */
	public void testConstructorNullName() {
		validTranscript = new Transcript(null);
	}

	@Test(groups = { "TestRanscript" }, expectedExceptions = IllegalArgumentException.class)
	/**
	 * This test will verify that a short name is rejected.
	 */
	public void testConstructorShortName() {
		validTranscript = new Transcript("Bo");
	}

	@DataProvider
	/**
	 * This data provider will be used to test GPA and other operations by combining terms and adding GPA's as is applicable.
	 * @return
	 */
	public Object[][] TermDataProvider() {
		Term t1 = new Term(AcademicQuarter.FALL, 2010); // Will have a GPA of
														// 3.19
		Term t2 = new Term(AcademicQuarter.WINTER, 2010); // Will have a GPA of
															// 3.20
		Term t3 = new Term(AcademicQuarter.FALL, 2011); // GPA of just below 3.7
		Term t4 = new Term(AcademicQuarter.SPRING, 2012); // GPA of just above
															// 3.7

		t1.addCourse(
				new CompletedCourse("SE1011 Programming One in Java and C++ and C# and Ruby and Assembly All in One",
						39, LetterGradeEnum.AB));
		t1.addCourse(new CompletedCourse("All the rest we need to know about software in one course", 61,
				LetterGradeEnum.B));

		t2.addCourse(new CompletedCourse("SE2832 Everything that is important about testing", 41, LetterGradeEnum.AB));
		t2.addCourse(
				new CompletedCourse("SE2833 Everything that is not important about testing", 59, LetterGradeEnum.B));

		t3.addCourse(new CompletedCourse("SDL", 39, LetterGradeEnum.A));
		t3.addCourse(new CompletedCourse("More SDL", 61, LetterGradeEnum.AB));

		t4.addCourse(new CompletedCourse("Senior Design", 41, LetterGradeEnum.A));
		t4.addCourse(new CompletedCourse("One Big Happy HUSS Course", 59, LetterGradeEnum.AB));

		return new Object[][] { new Object[] { new Term[] { t1 }, false, false },
				new Object[] { new Term[] { t2 }, false, true }, new Object[] { new Term[] { t3 }, false, true },
				new Object[] { new Term[] { t4 }, true, false },
				new Object[] { new Term[] { t1, t2, t3, t4 }, false, true },
				new Object[] { new Term[] { t1, t4 }, false, true }, };
	}

	@Test(groups = { "TestRanscript" }, dataProvider = "TermDataProvider")
	/**
	 * This method will ensure that the GPA calculation works right for multiple terms.
	 * @param terms These are the terms with classes.
	 * @param highHonors This is true if the GPA should be on high honors.
	 * @param deansList This si true if the GPA should be on the deans list.
	 */
	public void testGPA(Term[] terms, boolean highHonors, boolean deansList) {
		double expectedGPA = 0;
		int credits = 0;

		for (Term t : terms) {
			for (CompletedCourse cc : t.getCourses()) {
				expectedGPA += cc.getQualityPoints();
				credits += cc.getCourseCredits();
			}
			validTranscript.addTerm(t);
		}

		expectedGPA = expectedGPA / credits;

		assertEquals(validTranscript.calculateGPA(), expectedGPA, 0.01);
	}

	@Test(groups = { "TestRanscript" }, dataProvider = "TermDataProvider")
	/**
	 * This method will ensure that high honors are calculated properly.
	 * @param terms These are the terms with classes.
	 * @param highHonors This is true if the GPA should be on high honors.
	 * @param deansList This si true if the GPA should be on the deans list.
	 */
	public void testHighHonors(Term[] terms, boolean highHonors, boolean deansList) {
		for (Term t : terms) {
			validTranscript.addTerm(t);
		}
		assertEquals(validTranscript.isHighHonors(), highHonors);
	}

	@Test(groups = { "TestRanscript" }, dataProvider = "TermDataProvider")
	/**
	 * This method will ensure that deans list is processed properly.
	 * @param terms These are the terms with classes.
	 * @param highHonors This is true if the GPA should be on high honors.
	 * @param deansList This si true if the GPA should be on the deans list.
	 */
	public void testDeansList(Term[] terms, boolean highHonors, boolean deansList) {
		for (Term t : terms) {
			validTranscript.addTerm(t);
		}
		assertEquals(validTranscript.isDeansList(), deansList);
	}

	@Test(groups = { "TestRanscript" }, dataProvider = "TermDataProvider")
	/**
	 * This method will ensure that a transcript can be printed.
	 * @param terms These are the terms with classes.
	 * @param highHonors This is true if the GPA should be on high honors.
	 * @param deansList This si true if the GPA should be on the deans list.
	 */
	public void testPrintTranscript(Term[] terms, boolean highHonors, boolean deansList) {
		for (Term t : terms) {
			validTranscript.addTerm(t);
		}
		validTranscript.printTranscript();
	}
}
