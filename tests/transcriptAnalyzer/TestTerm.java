package transcriptAnalyzer;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;

/**
 * This class is responsible for verifying the correct operation of a given term.
 * @author schilling
 *
 */
public class TestTerm {
	@Test(groups = { "TestTerm" }, expectedExceptions = IllegalArgumentException.class)
	/**
	 * This will test that an invalid year is ignored.
	 */
	public void testInvalidYear() {
		new Term(AcademicQuarter.FALL, 1899);
	}

	@DataProvider
	/**
	 * This data provider will verify that the normal constructor behavior is proper.
	 * @return
	 */
	public Object[][] normalConstructorDataProvider() {
		return new Object[][] { 
			new Object[] { AcademicQuarter.FALL, 1900 },
			new Object[] { AcademicQuarter.WINTER, 1901 }, 
			new Object[] { AcademicQuarter.SPRING, 1902 },
			new Object[] { AcademicQuarter.SUMMER, 1903 } };
	}

	@Test(groups = { "TestTerm" }, dataProvider = "normalConstructorDataProvider")
	/**
	 * This test will assert that an object is properly constructed.
	 */
	public void testNormalConstructor(AcademicQuarter q, int year) {
		Term t = new Term(q, year);
		assertEquals(t.getYear(), year);
		assertEquals(t.getAcademicTerm(), q);
	}

	@Test(groups = { "TestTerm" })
	/**
	 * This test will verify that the most basic operation of adding a course to the term works properly.
	 */
	public void testAddCourseMostBasicTest() {
		Term t = new Term(AcademicQuarter.FALL, 2001);
		CompletedCourse cc = new CompletedCourse("Obilisque Studies on Jupiter", 5, LetterGradeEnum.C);
		assertTrue(t.addCourse(cc));
		assertTrue(t.getCourses().contains(cc));
		assertEquals(t.getCourses().size(), 1);
	}

	@Test(groups = { "TestTerm" })
	/**
	 * This test will ensure that trying to add a course which is already in the system will not be permitted.
	 */
	public void testAddCourseDuplicateTest() {
		Term t = new Term(AcademicQuarter.FALL, 2001);
		CompletedCourse cc = new CompletedCourse("Obilisque Studies on Jupiter", 5, LetterGradeEnum.C);
		assertTrue(t.addCourse(cc));
		assertEquals(t.getCourses().size(), 1);
		assertFalse(t.addCourse(cc));
		assertEquals(t.getCourses().size(), 1);
	}

	@Test(groups = { "TestTerm" })
	/**
	 * This tests verifies that multiple courses can be added to the system at once.
	 */
	public void testAddMultipleCourses() {
		Term t = new Term(AcademicQuarter.FALL, 2001);
		CompletedCourse ccset[] = {
				// Course name, credits, letter grade.
				new CompletedCourse("Obilisque Studies on Jupiter", 5, LetterGradeEnum.C),
				new CompletedCourse("Obilisque Studies on Moon", 2, LetterGradeEnum.AB) };

		t.addCourses(ccset);

		for (CompletedCourse cc : ccset) {
			assertTrue(t.getCourses().contains(cc));
		}
		assertEquals(t.getCourses().size(), 2);
	}

	@DataProvider
	/**
	 * This data provider verifies that the term GPA is handled properly by adding a set of courses 
	 * that a specific GPA has been set to for testing purposes. The format is a list of courses 
	 * followed by three booleans, one for good standing, one for probation, and one for honors.
	 */
	public Object[][] termGPADataProvider() {
		return new Object[][] { new Object[] {
				// The following is a 0.0 GPA.
				new CompletedCourse[] { new CompletedCourse("Obilisque Studies on Jupiter", 5, LetterGradeEnum.F),
						new CompletedCourse("Obilisque Studies on Moon", 2, LetterGradeEnum.F),
						new CompletedCourse("Obilisque Studies on Pluto", 5, LetterGradeEnum.F),
						new CompletedCourse("Obilisque Studies on Mars", 5, LetterGradeEnum.F), },
				false, true, false },

				new Object[] {
						// The following is a 4.0 GPA.
						new CompletedCourse[] {
								new CompletedCourse("Obilisque Studies on Jupiter", 5, LetterGradeEnum.A),
								new CompletedCourse("Obilisque Studies on Moon", 2, LetterGradeEnum.A),
								new CompletedCourse("Obilisque Studies on Pluto", 5, LetterGradeEnum.A),
								new CompletedCourse("Obilisque Studies on Mars", 5, LetterGradeEnum.A), },
						true, false, true },
				new Object[] {
						// The following is a 2.00 GPA.
						new CompletedCourse[] {
								new CompletedCourse("Obilisque Studies on Jupiter", 4, LetterGradeEnum.C),
								new CompletedCourse("Obilisque Studies on Moon", 1, LetterGradeEnum.C),
								new CompletedCourse("Obilisque Studies on Pluto", 2, LetterGradeEnum.C),
								new CompletedCourse("Obilisque Studies on Mars", 3, LetterGradeEnum.C), },
						true, false, false },

				new Object[] {
						// The following is a 2.01 GPA.
						new CompletedCourse[] {
								new CompletedCourse("Obilisque Studies on Jupiter", 99, LetterGradeEnum.C),
								new CompletedCourse("Obilisque Studies on Moon", 1, LetterGradeEnum.B),
								new CompletedCourse("Obilisque Studies on Jupiter", 0, LetterGradeEnum.A),
								new CompletedCourse("Obilisque Studies on Mars", 0, LetterGradeEnum.F), },
						true, false, false },

				new Object[] {
						// The following is a 1.99 GPA.
						new CompletedCourse[] {
								new CompletedCourse("Obilisque Studies on Jupiter", 99, LetterGradeEnum.C),
								new CompletedCourse("Obilisque Studies on Moon", 1, LetterGradeEnum.D),
								new CompletedCourse("Obilisque Studies on Jupiter", 0, LetterGradeEnum.A),
								new CompletedCourse("Obilisque Studies on Mars", 0, LetterGradeEnum.F), },
						false, true, false },

				new Object[] {
						// The following is a 3.20 GPA.
						new CompletedCourse[] {
								new CompletedCourse("Obilisque Studies on Jupiter", 80, LetterGradeEnum.A),
								new CompletedCourse("Obilisque Studies on Moon", 5, LetterGradeEnum.F),
								new CompletedCourse("Obilisque Studies on Jupiter", 5, LetterGradeEnum.F),
								new CompletedCourse("Obilisque Studies on Mars", 10, LetterGradeEnum.F), },
						true, false, true },
				new Object[] {
						// The following is a 3.19 GPA.
						new CompletedCourse[] {
								new CompletedCourse("Obilisque Studies on Jupiter", 79, LetterGradeEnum.A),
								new CompletedCourse("Obilisque Studies on Moon", 1, LetterGradeEnum.B),
								new CompletedCourse("Obilisque Studies on Jupiter", 10, LetterGradeEnum.F),
								new CompletedCourse("Obilisque Studies on Mars", 10, LetterGradeEnum.F), },
						true, false, false },
				new Object[] {
						// The following is a 3.21 GPA.
						new CompletedCourse[] {
								new CompletedCourse("Obilisque Studies on Jupiter", 80, LetterGradeEnum.A),
								new CompletedCourse("Obilisque Studies on Moon", 1, LetterGradeEnum.D),
								new CompletedCourse("Obilisque Studies on Jupiter", 9, LetterGradeEnum.F),
								new CompletedCourse("Obilisque Studies on Mars", 10, LetterGradeEnum.F), },
						true, false, true },

		};
	}

	@Test(groups = { "TestTerm" }, dataProvider = "termGPADataProvider")
	/**
	 * Verify that the term GPA is calculated properly.
	 */
	public void testTermGPA(CompletedCourse[] cc, boolean goodStanding, boolean probation, boolean honors) {
		Term t = new Term(AcademicQuarter.FALL, 2001);
		t.addCourses(cc);
		double gpa = 0;
		int credits = 0;

		for (CompletedCourse c : cc) {
			gpa += (c.getQualityPoints());
			credits += c.getCourseCredits();
		}
		gpa = gpa / credits;

		assertEquals(t.calculateTermGPA(), gpa, .001);
	}

	@Test(groups = { "TestTerm" }, dataProvider = "termGPADataProvider")
	/**
	 * Verify that in good standing is calculated properly.
	 */
	public void testTermisGoodStanding(CompletedCourse[] cc, boolean goodStanding, boolean probation, boolean honors) {
		Term t = new Term(AcademicQuarter.FALL, 2001);
		t.addCourses(cc);
		assertEquals(t.inGoodStanding(), goodStanding);
	}

	@Test(groups = { "TestTerm" }, dataProvider = "termGPADataProvider")
	/**
	 * Verify that probation is calculated properly.
	 */
	public void testTermisProbation(CompletedCourse[] cc, boolean goodStanding, boolean probation, boolean honors) {
		Term t = new Term(AcademicQuarter.FALL, 2001);
		t.addCourses(cc);
		assertEquals(t.isOnProbation(), probation);
	}

	@Test(groups = { "TestTerm" }, dataProvider = "termGPADataProvider")
	/**
	 * Verify that honors is calculated properly.
	 */
	public void testTermisHonors(CompletedCourse[] cc, boolean goodStanding, boolean probation, boolean honors) {
		Term t = new Term(AcademicQuarter.FALL, 2001);
		t.addCourses(cc);
		assertEquals(t.isOnHonorsList(), honors);
	}

	@Test(groups = { "TestTerm" }, dataProvider = "termGPADataProvider")
	/**
	 * Verify that term courses can be properly retrieved.
	 */
	public void testgetCourses(CompletedCourse[] cc, boolean goodStanding, boolean probation, boolean honors) {
		Term t = new Term(AcademicQuarter.FALL, 2001);
		t.addCourses(cc);
		assertEquals(t.getCourses().size(), cc.length);

		for (CompletedCourse c : cc) {
			assertTrue(t.getCourses().contains(c));
		}
	}

	@DataProvider
	/**
	 * This data provider will ensure that the comparison operation works properly.
	 * In short, given two instances, it will ensure that they are >, ==-, or <
	 * in the proper calcualtions. 
	 */
	public Object[][] comparisonDataProvider() {
		// These are a couple of term instances to be used for comparison purposes.
		Term t1 = new Term(AcademicQuarter.FALL, 1975);
		Term t2 = new Term(AcademicQuarter.WINTER, 1975);
		Term t3 = new Term(AcademicQuarter.FALL, 2000);
		Term t4 = new Term(AcademicQuarter.FALL, 2000);

		/** 
		 * Set of sets of them to use for comparison.  The third value is -1 if less than 0 if equal and 1 if greater than.
		 */
		return new Object[][] { 
			new Object[] { t1, t1, 0 }, 
			new Object[] { t1, t2, 1 }, 
			new Object[] { t2, t1, -1 },
			new Object[] { t1, t3, 1 }, 
			new Object[] { t3, t1, -1 }, 
			new Object[] { t3, t4, 0 }, };
	}

	@Test(groups = { "TestTerm" }, dataProvider = "comparisonDataProvider")
	public void testComparator(Term a, Term b, int expected) {
		assertEquals(a.compareTo(b), expected);
	}

}
