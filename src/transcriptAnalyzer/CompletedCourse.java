package transcriptAnalyzer;

import java.util.Objects;

/**
 * This class will hold information about a given course that has been
 * completed.
 * 
 * @author schilling
 *
 */
public class CompletedCourse {
	/**
	 * This is the name of the course.
	 */
	private String courseName;
	/**
	 * This is the number of credits for the given course. Must be positive.
	 */
	private int courseCredits;

	/**
	 * This is the letter grade for the given course.
	 */
	private LetterGradeEnum letterGrade;

	/**
	 * This will instantiate a new course.
	 * 
	 * @param courseName
	 *            This is the name of the course. Must be non-null and longer
	 *            than 1 character.
	 * @param courseCredits
	 *            This is the credits for the given course. Must be positive.
	 * @param letterGrade
	 *            This is the letter grade the student received in the course.
	 * @throws IllegalArgumentException
	 *             This will be thrown if the course name is invalid or the
	 *             credits is out of range.
	 */
	public CompletedCourse(String courseName, int courseCredits, LetterGradeEnum letterGrade)
			throws IllegalArgumentException {
		super();
		if (courseName.length() < 2) {
			throw new IllegalArgumentException("Course Name must be 2 or more characters in length.");
		}

		if (courseCredits < 0) {
			throw new IllegalArgumentException("Credits must be 0 or greater.");
		}

		this.courseName = courseName;
		this.courseCredits = courseCredits;
		this.letterGrade = letterGrade;
	}

	/**
	 * This will obtain the course name for the given course.
	 * 
	 * @return
	 */
	public String getCourseName() {
		return courseName;
	}

	/**
	 * This method will return the course credits for the given course.
	 * 
	 * @return
	 */
	public int getCourseCredits() {
		return courseCredits;
	}

	/**
	 * This method will return the letter grade for the given course.
	 * 
	 * @return
	 */
	public LetterGradeEnum getLetterGrade() {
		return letterGrade;
	}

	/**
	 * This method will return the quality points earned for this course.
	 * 
	 * @return The number of quality points is the letter grade quality points
	 *         multiplied by the number of credits.
	 */
	public double getQualityPoints() {
		return letterGrade.getQualityPoints() * courseCredits;
	}

	@Override
	public boolean equals(Object o) {
		// If the object is compared with itself then return true
		if (o == this) {
			return true;
		}

		/*
		 * Check if o is an instance of Complex or not "null instanceof [type]"
		 * also returns false
		 */
		if (!(o instanceof CompletedCourse)) {
			return false;
		}

		CompletedCourse cc = (CompletedCourse) o;
		if ((cc.courseCredits == this.courseCredits) && (cc.courseName.equals(this.courseName))
				&& (cc.letterGrade == this.letterGrade)) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public int hashCode() {
		return Objects.hash(this.courseCredits, this.courseName, this.letterGrade);
	}

}
