rem this script will execute the pit test program on a given fileset using TestNG.
set local

rem The following definition defines which classes are to be tested.  It should include the java package to the classes.
set targetClasses="transcriptAnalyzer.*"

rem The following line defines the tests which are to be run.  It should include the full package of the tests that are to be executed.
set targettests="transcriptAnalyzer.*"

rem This definition defines the classpath that is to be used to test the system.
set cp="bin;testng.jar;pitest-1.1.11.jar;pitest-command-line-1.1.11.jar"

rem This folder defines the relative path to the source.
set sd="src"

rem This definition defines which mutators are to be used to test the program.
set mutators="INCREMENTS,REMOVE_CONDITIONALS,VOID_METHOD_CALLS,NON_VOID_METHOD_CALLS,EXPERIMENTAL_SWITCH,CONSTRUCTOR_CALLS,INLINE_CONSTS,RETURN_VALS,MATH,NEGATE_CONDITIONALS,INVERT_NEGS,CONDITIONALS_BOUNDARY"

rem This definition defines where the output is to be placed.
set reportdir="pitResults"

rem This definition defines the output formats.
set outputformat="html,xml,csv"

rem Actually execute the pittest tool.
java -cp %cp% org.pitest.mutationtest.commandline.MutationCoverageReport --reportDir %reportdir% --targetClasses="%targetclasses%" --targetTests="%targettests%" --mutators=%mutators% --outputFormats=%outputformat% --sourceDirs %sd%
endlocal


